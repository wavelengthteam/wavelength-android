package com.wavelength;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.wavelength.models.WLUser;
import com.wavelength.listen.ListenWaveFragment;
import com.wavelength.search.SearchActivity;
import com.wavelength.start.LoginActivity;
import com.wavelength.start.StartWaveFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * The main activity, which handles moving between screens from the navigation drawer. Begins by
 * running launching the core wave feature set up for the user.
 */
public class NavigationActivity extends AppCompatActivity implements
        StartWaveFragment.OnStartWaveDecisionSelectedListener,
        ListenWaveFragment.OnListenWaveDecisionSelectedListener {

    // region Variables

    private static final int SEARCH_ACTIVITY_REQUEST_CODE = 1;

    @Bind(R.id.navigation_drawer_layout) DrawerLayout navigationLayout;
    @Bind(R.id.navigation_view) NavigationView navigationView;
    @Bind(R.id.nav_header_user_photo) ImageView navigationHeaderUserImage;
    @Bind(R.id.nav_header_user_name) TextView navigationHeaderUserName;
    @Bind(R.id.main_toolbar) Toolbar toolbar;

    // Fragment Handling
    FragmentManager fragmentManager;
    private StartWaveFragment startWaveFragment;
    private ListenWaveFragment listenWaveFragment;

    private boolean isPremium = false;
    private String accessToken;

    // endregion

    // region Fundamentals

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);

        // Obtain Spotify Access Token from bundle
        accessToken = getIntent().getStringExtra(LoginActivity.ACCESS_TOKEN);

        // Obtain whether the user is premium
        isPremium = getIntent().getBooleanExtra(LoginActivity.IS_PREMIUM, false);

        // Initial UI Setup
        setUpActionBar();
        setupDrawerContent();

        // Set up fragment system
        startWaveFragment = new StartWaveFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_container, startWaveFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // we've received the list of search results, so we launch the play music screen
                ArrayList<WLUser> searchResults = data.getParcelableArrayListExtra(SearchActivity.BUNDLE_SEARCH_RESULTS_KEY);
                launchPlayScreen(searchResults);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnToAndroidHome = new Intent(Intent.ACTION_MAIN);
        returnToAndroidHome.addCategory(Intent.CATEGORY_HOME);
        returnToAndroidHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(returnToAndroidHome);
    }

    // endregion

    // region Setup

    /**
     * Sets up custom toolbar as actionbar
     */
    private void setUpActionBar() {
        // As we're using a Toolbar, we should retrieve it and set it to be our ActionBar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /**
     * Sets up the views and handling of the views inside of the navigation drawer.
     */
    private void setupDrawerContent() {
        // Set up Header in navigation drawer
        WLUser currentUser = WLUser.getCurrentUser(this);
        if (currentUser == null) {
            Log.e("NavigationActivity", "User does not exist. Must relay back to LoginActivity");
        } else {
            navigationHeaderUserName.setText(currentUser.getName());
            Glide.with(this).load(currentUser.getImageUrl()).asBitmap().centerCrop().into(navigationHeaderUserImage);
        }

        // set up clicking on items
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    /**
     * Depending on what is clicked in the navigation drawer, a different action occurs.
     * @param menuItem The item chosen in the navigation drawer.
     */
    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        // TODO launch dialogs
        switch(menuItem.getItemId()) {
//            case R.id.nav_about:
//                swapFragment(startWaveFragment);
//                break;
            case R.id.nav_logout:
                logout();
                break;
            default:
                swapFragment(startWaveFragment);
        }

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        navigationLayout.closeDrawers();
    }

    // endregion

    // region Callbacks

    /**
     * Launches the Activity to begin creating a wave with other users.
     */
    @Override
    public void onCreateWaveSelected() {
        if (isPremium) {
            Intent intent = new Intent(NavigationActivity.this, SearchActivity.class);
            startActivityForResult(intent, SEARCH_ACTIVITY_REQUEST_CODE);
        } else {
            displayNotPremiumDialog();
        }
    }

    private void launchPlayScreen(ArrayList<WLUser> waveGroup) {
        Bundle args = new Bundle();
        args.putString(LoginActivity.ACCESS_TOKEN, accessToken);
        // TODO don't use hardcoded strings
        args.putParcelableArrayList("users", waveGroup);
        if (listenWaveFragment == null) {
            listenWaveFragment = new ListenWaveFragment();
        }
        listenWaveFragment.setArguments(args);
        fragmentManager.beginTransaction().replace(R.id.content_container, listenWaveFragment).commit();
    }

    @Override
    public void onEndWaveSelected() {
        fragmentManager.beginTransaction().replace(R.id.content_container, startWaveFragment).commit();
    }

    // endregion

    // region Helpers

    private void swapFragment(Fragment fragment) {
        // Insert the fragment by replacing any existing fragment
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_container, fragment).commit();
    }

    private void logout() {
        AuthenticationClient.logout(this);
        WLUser.clearCurrentUser(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        // because the user has requested to logout, we should not attempt to autologin upon
        // returning to the login screen
        intent.putExtra(LoginActivity.AUTO_LOGIN, false);
        startActivity(intent);
        finish();
    }

    private void displayNotPremiumDialog() {
        new AlertDialog.Builder(this)
            .setTitle(R.string.start_not_premium_dialog_title)
            .setMessage(R.string.start_not_premium_dialog_message)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    // endregion

    // region Interactions

    @OnClick(R.id.main_toolbar_navigation_toggle)
    public void onNavigationToggleClicked() {
        navigationLayout.openDrawer(GravityCompat.START);
    }

    // endregion
}
