package com.wavelength;

import android.app.Application;
import android.content.SharedPreferences;

import com.parse.Parse;
import com.parse.ParseObject;
import com.wavelength.models.WLUser;
import com.wavelength.utils.FontsOverrideUtils;

/**
 * Created by nikhilbedi on 9/16/15.
 */
public class WavelengthApplication extends Application {

    public static final String PREFS_SETTINGS_FILE_NAME = "Settings";
    public static final String PREFS_SETTINGS_FIRST_TIME_KEY = "first_time";

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Register parse subclasses (must be called before initialize)
        ParseObject.registerSubclass(WLUser.class);

        Parse.initialize(this, getString(R.string.parse_application_id), getString(R.string.parse_client_key));

        overrideDefaultFonts();

        setupInitialSettingsPreferences();
    }

    private void overrideDefaultFonts() {
        FontsOverrideUtils.setDefaultFont(this, "SANS_SERIF", "fonts/ProximaNova-Regular.ttf");
    }

    private void setupInitialSettingsPreferences() {
        SharedPreferences settings = getSharedPreferences(PREFS_SETTINGS_FILE_NAME, 0);

        if (settings.getBoolean(PREFS_SETTINGS_FIRST_TIME_KEY, true)) {
            // record the fact that the app has been started at least once
            settings.edit().putBoolean(PREFS_SETTINGS_FIRST_TIME_KEY, false).apply();
        }
    }

    public boolean isInitialAppLaunch() {
        return getSharedPreferences(PREFS_SETTINGS_FILE_NAME, 0)
                .getBoolean(PREFS_SETTINGS_FIRST_TIME_KEY, true);
    }

    public void setInitialAppLaunch(boolean isAppLaunchFirstTime) {
        getSharedPreferences(PREFS_SETTINGS_FILE_NAME, 0)
                .edit()
                .putBoolean(PREFS_SETTINGS_FIRST_TIME_KEY, isAppLaunchFirstTime)
                .apply();
    }
}
