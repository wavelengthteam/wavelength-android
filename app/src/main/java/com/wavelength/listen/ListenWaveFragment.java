package com.wavelength.listen;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.PlayConfig;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.Spotify;
import com.wavelength.R;
import com.wavelength.models.WLUser;
import com.wavelength.start.LoginActivity;
import com.wavelength.utils.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.ArtistSimple;
import kaaes.spotify.webapi.android.models.Track;
import kaaes.spotify.webapi.android.models.Tracks;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * The main listening screen for handling music playback.
 */
public class ListenWaveFragment extends Fragment implements PlayerNotificationCallback {

    // region Variables

    public static final String TAG = "ListenWaveFragment";

    // Views
    @Bind(R.id.listen_song_image) ImageView songImageView;
    @Bind(R.id.listen_play_button_image) ImageView playButtonImageView;
    @Bind(R.id.listen_desc_song_title) TextView songTitleTextView;
    @Bind(R.id.listen_desc_artist_name) TextView artistNameTextView;
    @Bind(R.id.listen_song_progress) ProgressBar trackProgressBar;
    ProgressDialog buildWaveProgressDialog;

    // Spotify
    SpotifyApi spotifyApi;
    SpotifyService spotifyService;
    Player spotifyPlayer;

    // Variables to keep track of current tracks
    private HashMap<String, Track> currentWave;
    private boolean isCurrentlyPlaying = false;
    private int currentTrackProgress = 0;
    private int trackProgressMax = 100;
    private Handler seekBarProgressHandler = new Handler();
    Runnable updateTrackProgressRunnable = new Runnable() {
        @Override
        public void run() {
            trackProgressBar.setProgress(++currentTrackProgress);
            seekBarProgressHandler.postDelayed(updateTrackProgressRunnable, 1000);
        }
    };

    OnListenWaveDecisionSelectedListener activityCallback;

    // endregion

    // region Interfaces
    /**
     * Activity associated with this fragment must implement this listener in order for this fragment
     * to properly relay information.
     */
    public interface OnListenWaveDecisionSelectedListener {
        void onEndWaveSelected();
    }

    // endregion

    // region Fundamentals

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This forces the activity instantiating this class to implement the corresponding listener
        // needed for this fragment to properly send events
        try {
            activityCallback = (OnListenWaveDecisionSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnListenWaveDecisionSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listen_wave_fragment, container, false);
        ButterKnife.bind(this, view);

        setUpSpotifyService();
        setUpSpotifyPlayer();

        // Show Progress Dialog until a wave is returned
        buildWaveProgressDialog = ProgressDialog.show(
                getActivity(),
                getString(R.string.listen_build_wave_dialog_title),
                getString(R.string.listen_build_wave_dialog_message),
                true,
                false);

        // Obtain list of track URIs and obtain full track information
        // TODO don't use hardcoded strings
        ArrayList<WLUser> users = getArguments().getParcelableArrayList("users");
        String[] userIds = new String[users.size()];
        for (int i = 0; i < users.size(); i++) {
            userIds[i] = users.get(i).getSpotifyId();
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", getArguments().getString(LoginActivity.ACCESS_TOKEN));
        params.put("data", Arrays.asList(userIds));
        ParseCloud.callFunctionInBackground("BuildWave", params, new FunctionCallback<List<String>>() {
            @Override
            public void done(List<String> trackIds, ParseException e) {
                if (e == null) {
                    buildWaveProgressDialog.setMessage(
                            getString(R.string.listen_build_wave_dialog_message_secondary));
                    setUpTracksAndPlay(trackIds);
                } else {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        spotifyPlayer.pause();
        spotifyPlayer.clearQueue();
        super.onDetach();
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        switch(eventType) {
            // Ensure proper UI is displayed
            case PLAY:
                if (buildWaveProgressDialog.isShowing()) {
                    buildWaveProgressDialog.dismiss();
                }
                isCurrentlyPlaying = true;
                currentTrackProgress = millisecondsToSeconds(playerState.positionInMs);
                seekBarProgressHandler.removeCallbacks(updateTrackProgressRunnable);
                seekBarProgressHandler.postDelayed(updateTrackProgressRunnable, 1000);
                // We need to check if the fragment is still connected to the activity, since we use
                // getResources()
                if (isAdded()) {
                    playButtonImageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_pause_black));
                }
                Log.d("Nikhil", "Playing track " + playerState.trackUri);
                break;

            case PAUSE:
                isCurrentlyPlaying = false;
                currentTrackProgress = millisecondsToSeconds(playerState.positionInMs);
                seekBarProgressHandler.removeCallbacks(updateTrackProgressRunnable);
                // We need to check if the fragment is still connected to the activity, since we use
                // getResources()
                if (isAdded()) {
                    playButtonImageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_play_black));
                }
                Log.d("Nikhil", "Pausing track " + playerState.trackUri);
                break;

            // Handle modifying UI signatures for new track
            case TRACK_CHANGED:
                currentTrackProgress = 0;
                trackProgressMax = millisecondsToSeconds(playerState.durationInMs);
                trackProgressBar.setMax(trackProgressMax);
                displayTrackInformation(currentWave.get(playerState.trackUri));
                Log.d("Nikhil", "Changing track to " + playerState.trackUri);
                break;

            // Handle user losing permission due to playing music from another device
            case LOST_PERMISSION:
                Log.e(TAG, "Cannot play music. The user must have started playing from another device");
                break;

            // Handle the wave ending. Lead user to create a new wave
            case END_OF_CONTEXT:
                Log.d("Nikhil", "Wave has reached its end of context");
                break;
        }
    }

    @Override
    public void onPlaybackError(ErrorType errorType, String s) {
        Log.e(TAG, errorType.name() + " " + s);
    }

    // endregion

    // region Interactions

    @OnClick(R.id.listen_play_button)
    public void onPlayButtonClicked() {
        if (isCurrentlyPlaying) {
            spotifyPlayer.pause();
        } else {
            spotifyPlayer.resume();
        }
        isCurrentlyPlaying = !isCurrentlyPlaying;
    }

    @OnClick(R.id.listen_prev_button)
    public void onPreviousButtonClicked() {
        spotifyPlayer.skipToPrevious();
    }

    @OnClick(R.id.listen_next_button)
    public void onNextButtonClicked() {
        spotifyPlayer.skipToNext();
    }

    @OnClick(R.id.listen_end_wave_button)
    public void onEndWaveButtonClicked() {
        activityCallback.onEndWaveSelected();
    }

    // endregion

    // region Helpers

    private void setUpSpotifyService() {
        spotifyApi = new SpotifyApi();
        spotifyApi.setAccessToken(getArguments().getString(LoginActivity.ACCESS_TOKEN));
        spotifyService = spotifyApi.getService();
    }

    private void setUpTracksAndPlay(List<String> trackUris) {
        List<String> tracks = new ArrayList<>();
        // Alert user if there is no tracks to play.
        if (trackUris.isEmpty()) {
            new AlertDialog.Builder(this.getContext())
                .setTitle(getString(R.string.listen_error_dialog_title))
                .setMessage(getString(R.string.listen_error_dialog_title))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            buildWaveProgressDialog.dismiss();
            // TODO on dismiss, return to previous screen
        }

        // randomize order of list
        Collections.shuffle(trackUris);

        // TODO no magic numbers
        // 50 is the max spotify allows to play a sequence of songs
        if (trackUris.size() > 50) {
            for (int i = 0; i < 50; i++) {
                tracks.add(trackUris.get(i));
            }
        }
        spotifyService.getTracks(TextUtils.createCommaSeparatedStringFromList(tracks), new Callback<Tracks>() {
            @Override
            public void success(Tracks tracks, Response response) {
                currentWave = new HashMap<String, Track>();
                // remove all tracks that were null
                tracks.tracks.removeAll(Collections.singleton(null));
                if (!tracks.tracks.isEmpty()) {
                    ArrayList<String> trackUris = new ArrayList<>();
                    for (Track track : tracks.tracks) {
                        trackUris.add(track.uri);
                        currentWave.put(track.uri, track);
                    }
                    spotifyPlayer.play(PlayConfig.createFor(trackUris));
                    isCurrentlyPlaying = true;
                    spotifyPlayer.setRepeat(true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void setUpSpotifyPlayer() {
        Config playerConfig = new Config(getContext(),
                getArguments().getString(LoginActivity.ACCESS_TOKEN),
                getString(R.string.spotify_client_key));
        spotifyPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
            @Override
            public void onInitialized(Player player) {
                if (spotifyPlayer != null) {
                    spotifyPlayer.addPlayerNotificationCallback(ListenWaveFragment.this);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // TODO notify user music could not be played
                Log.e(TAG, "Could not initialize player: " + throwable.getMessage());
            }
        });
    }

    private void displayTrackInformation(Track track) {
        songTitleTextView.setText(track.name);
        artistNameTextView.setText(createArtistTitle(track.artists));
        if (!track.album.images.isEmpty()) {
            Glide.with(this).load(track.album.images.get(0).url).into(songImageView);
        }
    }

    private String createArtistTitle(List<ArtistSimple> artists) {
        StringBuilder sb = new StringBuilder();
        for (ArtistSimple artist : artists) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(artist.name);
        }
        return sb.toString();
    }

    private int millisecondsToSeconds(long ms) {
        return (int) (ms / 1000.0);
    }

    // endregion

}
