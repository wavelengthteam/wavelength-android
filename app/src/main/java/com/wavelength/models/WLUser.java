package com.wavelength.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wavelength.R;

/**
 * An enhanced model to hold WLUser data and also store/retrieve information about the currently
 * logged in user.
 */
@ParseClassName("WLUser")
public class WLUser extends ParseObject implements Parcelable {

    private static WLUser currentUser;

    /*
    Parse Class Name
     */
    public static final String CLASS_NAME = "WLUser";

    /*
    Parse user keys
     */
    public static final String KEY_NAME = "name";
    public static final String KEY_SPOTIFY_ID = "spotifyId";
    public static final String KEY_IMAGE_URL = "profileImageUrl";
    public static final String KEY_DEVICE_ID = "deviceId";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_KEYWORDS = "keywords";
    public static final String KEY_FOLLOWING = "following";
    public static final String KEY_OBJECT_ID = "objectId";

    // region Fundamentals

    public WLUser() {
        super();
    }

    public WLUser(@Nullable String name,
                  @NonNull String spotifyId,
                  @Nullable String email,
                  @Nullable String profileImageUrl,
                  @Nullable String deviceId) {
        super(CLASS_NAME);
        put(KEY_SPOTIFY_ID, spotifyId);
        if (name != null) {
            put(KEY_NAME, name);
        }
        if (profileImageUrl != null) {
            put(KEY_IMAGE_URL, profileImageUrl);
        }
        if (deviceId != null) {
            put(KEY_DEVICE_ID, deviceId);
        }
        if (email != null) {
            put(KEY_EMAIL, email);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getName());
        dest.writeString(getSpotifyId());
        dest.writeString(getEmail());
        dest.writeString(getImageUrl());
        dest.writeString(getDeviceId());
    }

    public static final Parcelable.Creator<WLUser> CREATOR
            = new Parcelable.Creator<WLUser>() {
        public WLUser createFromParcel(Parcel in) {
            return new WLUser(in.readString(), in.readString(), in.readString(), in.readString(), in.readString());
        }

        public WLUser[] newArray(int size) {
            return new WLUser[size];
        }
    };

    // endregion

    public static WLUser createWithObjectId(@NonNull String objectId,
                                            @Nullable String name,
                                            @NonNull String spotifyId,
                                            @Nullable String email,
                                            @Nullable String profileImageUrl,
                                            @Nullable String deviceId) {
        WLUser user = ParseObject.createWithoutData(WLUser.class, objectId);
        user.put(KEY_SPOTIFY_ID, spotifyId);
        if (name != null) {
            user.put(KEY_NAME, name);
        }
        if (profileImageUrl != null) {
            user.put(KEY_IMAGE_URL, profileImageUrl);
        }
        if (deviceId != null) {
            user.put(KEY_DEVICE_ID, deviceId);
        }
        if (email != null) {
            user.put(KEY_EMAIL, email);
        }
        return user;
    }

    /**
     * Constructs a query for {@code WLUser}.
     *
     * @see com.parse.ParseQuery#getQuery(Class)
     */
    public static ParseQuery<WLUser> getQuery() {
        return ParseQuery.getQuery(WLUser.class);
    }


    public static void setCurrentUser(@NonNull Context context, @NonNull WLUser user) {
        currentUser = user;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_wluser_file_key),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.preference_wluser_name), user.get(KEY_NAME).toString());
        editor.putString(context.getString(R.string.preference_wluser_spotify_id), user.get(KEY_SPOTIFY_ID).toString());
        if (user.get(KEY_IMAGE_URL) != null) {
            editor.putString(context.getString(R.string.preference_wluser_image_url), user.get(KEY_IMAGE_URL).toString());
        }
        editor.putString(context.getString(R.string.preference_wluser_object_id), user.getObjectId());
        editor.apply();
    }

    /**
     * Obtains the current user either from a local variable, or via Android's SharedPreferences. To
     * be considered a valid user, only a spotify ID is required.
     * @param context Used to interface with Android to obtain stored information
     * @return the current user, or null if not found.
     */
    public static WLUser getCurrentUser(@NonNull Context context) {
        if (currentUser != null) {
            return currentUser;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_wluser_file_key),
                Context.MODE_PRIVATE);
        String name = sharedPreferences.getString(context.getString(R.string.preference_wluser_name), null);
        String spotifyId = sharedPreferences.getString(context.getString(R.string.preference_wluser_spotify_id), null);
        String imageUrl = sharedPreferences.getString(context.getString(R.string.preference_wluser_image_url), null);
        String objectId = sharedPreferences.getString(context.getString(R.string.preference_wluser_object_id), null);

        if (spotifyId == null || objectId == null) {
            return null;
        }
        currentUser = WLUser.createWithObjectId(objectId, name, spotifyId, null, imageUrl, null);
        return currentUser;
    }

    public static void clearCurrentUser(@NonNull Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_wluser_file_key),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(context.getString(R.string.preference_wluser_spotify_id));
        editor.clear();
        editor.commit();
    }

    @Nullable
    public String getName() {
        return getString(KEY_NAME);
    }

    @NonNull
    public String getSpotifyId() {
        return getString(KEY_SPOTIFY_ID);
    }

    @Nullable
    public String getImageUrl() {
        return getString(KEY_IMAGE_URL);
    }

    public void setImageUrl(String imageUrl) {
        if (imageUrl != null) {
            put(KEY_IMAGE_URL, imageUrl);
        }
    }

    @Nullable
    public String getEmail() {
        return getString(KEY_EMAIL);
    }

    @Nullable
    public String getDeviceId() {
        return getString(KEY_DEVICE_ID);
    }
}
