package com.wavelength.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.nearby.messages.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

/**
 * Used to prepare the payload for a
 * {@link com.google.android.gms.nearby.messages.Message Nearby Message}. Adds a unique id (an
 * InstanceID) to the Message payload, which helps Nearby distinguish between multiple devices with
 * the same model name.
 */
public class WaveMessage {
    private static final String KEY_INSTANCE_ID = "instanceId";
    private final String instanceId;
    private final String userName;
    private final String userSpotifyId;
    private final String userImageUrl;
    private final String userObjectId;

    /**
     * Builds a new {@link Message} object using a unique identifier, and information about the
     * WLUser.
     */
    public static Message newNearbyMessage(@NonNull String instanceId,
                                           @Nullable String userName,
                                           @NonNull String userSpotifyId,
                                           @Nullable String userImageUrl,
                                           @NonNull String userObjectId) {
        JSONObject payload = new JSONObject();
        try {
            payload.put(KEY_INSTANCE_ID, instanceId);
            payload.put(WLUser.KEY_NAME, userName);
            payload.put(WLUser.KEY_SPOTIFY_ID, userSpotifyId);
            payload.put(WLUser.KEY_IMAGE_URL, userImageUrl);
            payload.put(WLUser.KEY_OBJECT_ID, userObjectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new Message(payload.toString().getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Creates a {@code WaveMessage} object from the string used to construct the payload to a
     * {@code Nearby} {@code Message}.
     */
    public static WaveMessage fromNearbyMessage(Message message) {
        String nearbyMessageString = new String(message.getContent()).trim();
        String instanceId = null;
        String name = null;
        String spotifyId = null;
        String imageUrl = null;
        String objectId = null;
        try {
            JSONObject payload = new JSONObject(nearbyMessageString);
            objectId = payload.getString(WLUser.KEY_OBJECT_ID);
            instanceId = payload.getString(KEY_INSTANCE_ID);
            name = payload.getString(WLUser.KEY_NAME);
            spotifyId = payload.getString(WLUser.KEY_SPOTIFY_ID);
            imageUrl = payload.getString(WLUser.KEY_IMAGE_URL);
            return new WaveMessage(instanceId, name, spotifyId, imageUrl, objectId);
        } catch (JSONException e) {
            if (instanceId != null && name != null && spotifyId != null && objectId != null) {
                return new WaveMessage(instanceId, name, spotifyId, null, objectId);
            } else {
                e.printStackTrace();
            }
        }
        return null;
    }

    private WaveMessage(String instanceId, String userName, String userSpotifyId, String userImageUrl, String userObjectId) {
        this.instanceId = instanceId;
        this.userName = userName;
        this.userSpotifyId = userSpotifyId;
        this.userImageUrl = userImageUrl;
        this.userObjectId = userObjectId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String getName() {
        return userName;
    }

    public String getSpotifyId() {
        return userSpotifyId;
    }

    public String getImageUrl() {
        return userImageUrl;
    }

    public String getObjectId() {
        return userObjectId;
    }
}
