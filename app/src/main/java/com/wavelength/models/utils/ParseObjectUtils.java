package com.wavelength.models.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

/**
 * A helper class to centralize Parse object information and abstract away basic Parse cloud calls.
 */
public class ParseObjectUtils {
    /*
    Parse Class Objects
     */
    private static final String USER_CLASS_NAME = "WLUser";

    /*
    Parse user keys
     */
    private static final String USER_KEY_NAME = "name";
    private static final String USER_KEY_SPOTIFY_ID = "spotifyId";
    private static final String USER_KEY_IMAGE_URL = "profileImageUrl";

    @Deprecated
    /**
     * This function is now deprecated. Please use WLUser constructor.
     */
    public static void createNewUser(@NonNull String name,
                                     @NonNull String spotifyId,
                                     @Nullable String imageUrl) {
        ParseObject newUserObject = new ParseObject(USER_CLASS_NAME);
        newUserObject.put(USER_KEY_NAME, name);
        newUserObject.put(USER_KEY_SPOTIFY_ID, spotifyId);
        newUserObject.put(USER_KEY_IMAGE_URL, imageUrl);
        newUserObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });
    }

}
