package com.wavelength.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wavelength.R;
import com.wavelength.models.WLUser;
import com.wavelength.utils.TextUtils;

import java.util.List;

/**
 * Adapter for displaying results of WLUsers as a horizontal list of circular pictures in a
 * {@link RecyclerView}.
 */
public class HorizontalBubbleRecyclerViewAdapter
        extends RecyclerView.Adapter<HorizontalBubbleRecyclerViewAdapter.ViewHolder> {

    // region Variables

    private static int MAX_NAME_LENGTH = 8;

    private List<WLUser> values;
    private Context context;
    private RecyclerViewItemClickListener itemListener;

    // endregion

    // region Interfaces

    /**
     * A listener for the context that instantiates {@link HorizontalBubbleRecyclerViewAdapter}, so that the
     * context can catch item clicks in the connected {@link RecyclerView}.
     */
    public interface RecyclerViewItemClickListener {
        void horizontalBubbleRecyclerViewItemClicked(View v, int position);
    }

    // endregion

    // region Fundamentals

    public HorizontalBubbleRecyclerViewAdapter(@NonNull Context context,
                                               @NonNull List<WLUser> items,
                                               @NonNull RecyclerViewItemClickListener itemListener) {
        this.values = items;
        this.context = context;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_wave_list_item, parent, false);
        return new ViewHolder(view, itemListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        WLUser user = values.get(position);
        if (user.getName() != null) {
            viewHolder.personName.setText(TextUtils.ellipsize(user.getName(), MAX_NAME_LENGTH));
        } else {
            viewHolder.personName.setText(user.getSpotifyId());
        }
        Glide.with(context)
                .load(user.getImageUrl())
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.ghost_avatar)
                .into(viewHolder.personImage);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public RecyclerViewItemClickListener listener;
        public final View view;
        public final TextView personName;
        public final ImageView personImage;

        public ViewHolder(View view, RecyclerViewItemClickListener listener) {
            super(view);
            this.listener = listener;
            this.view = view;
            personName = (TextView) view.findViewById(R.id.wave_item_person_name);
            personImage = (ImageView) view.findViewById(R.id.wave_item_person_photo);
            this.view.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " " + personName.getText();
        }

        @Override
        public void onClick(View v) {
            try {
                listener.horizontalBubbleRecyclerViewItemClicked(v, getLayoutPosition());
            } catch (NullPointerException e) {
                Log.e("SearchPeopleResults", "No item click listener is added and thereby null.");
                e.printStackTrace();
            }
        }
    }

    // endregion
}
