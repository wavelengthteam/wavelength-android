package com.wavelength.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.wavelength.R;
import com.wavelength.models.WLUser;
import com.wavelength.utils.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * The activity which manages user interface and functionality for searching
 * for other users and sending chosen users back to a previous activity.
 */
public class SearchActivity extends AppCompatActivity
        implements HorizontalBubbleRecyclerViewAdapter.RecyclerViewItemClickListener,
        SearchRecyclerViewAdapter.RecyclerViewItemClickListener {

    // region Variables

    public static final String TAG = "SearchActivity";
    public static final String BUNDLE_SEARCH_RESULTS_KEY = "searchResults";
    private static final String SNACKBAR_RESPONSE = "User Added";
    private static final String SNACKBAR_ACTION = "UNDO";
    private static final String HELP_TEXT_TARGET = "Click here";

    @Bind(R.id.search_toolbar) Toolbar toolbar;
    @Bind(R.id.search_coordinator_layout) CoordinatorLayout coordinatorLayout;
    @Bind(R.id.search_people_list) RecyclerView peopleRecyclerView;
    @Bind(R.id.search_wave_list) RecyclerView waveRecyclerView;
    @Bind(R.id.search_bar) EditText searchBarEditText;
    @Bind(R.id.search_wave_help_text) TextView waveHelpText;
    @Bind(R.id.search_help_text) TextView helpFindFriendsText;
    @Bind(R.id.search_people_list_title_textview) TextView peopleListTitleView;

    private ArrayList<WLUser> selectedUsersForWave = new ArrayList<>();
    private ArrayList<WLUser> searchResults = new ArrayList<>();
    private SearchRecyclerViewAdapter searchRecyclerViewAdapter;
    private HorizontalBubbleRecyclerViewAdapter waveRecyclerViewAdapter;

    // endregion

    // region Fundamentals

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setUpRecyclerViews();
        setUpActionBar();
        setUpSearchBar();
        setUpHelpText();
        triggerRandomQuery();
    }

    // endregion

    // region Helpers

    private void setUpRecyclerViews() {
        setUpWaveRecyclerView();
        setUpPeopleRecyclerView();
    }

    private void setUpWaveRecyclerView() {
        waveRecyclerView.setLayoutManager(new LinearLayoutManager(waveRecyclerView.getContext(),
                LinearLayoutManager.HORIZONTAL,
                false));
        waveRecyclerViewAdapter = new HorizontalBubbleRecyclerViewAdapter(this.getBaseContext(),
                selectedUsersForWave,
                this);
        waveRecyclerView.setAdapter(waveRecyclerViewAdapter);
    }

    private void setUpPeopleRecyclerView() {
        peopleRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        peopleRecyclerView.setLayoutManager(new LinearLayoutManager(peopleRecyclerView.getContext()));
        peopleRecyclerView.setItemAnimator(new DefaultItemAnimator());
        searchRecyclerViewAdapter = new SearchRecyclerViewAdapter(this.getBaseContext(), searchResults, this);
        peopleRecyclerView.setAdapter(searchRecyclerViewAdapter);
    }

    /**
     * Sets up custom toolbar as actionbar
     */
    private void setUpActionBar() {
        // As we're using a Toolbar, we should retrieve it and set it to be our ActionBar
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    private void setUpSearchBar() {
        searchBarEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchButtonClicked();
                }
                return false;
            }
        });
    }

    private void setUpHelpText() {
        final Context context = this;
        SpannableString str = TextUtils.applyClickableSpan(helpFindFriendsText.getText().toString(),
                HELP_TEXT_TARGET,
                new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                final TextView message = new TextView(context);
                final SpannableString s =
                        new SpannableString(context.getText(R.string.search_help_find_friends_dialog_body));
                Linkify.addLinks(s, Linkify.WEB_URLS);
                message.setText(s);
                message.setMovementMethod(LinkMovementMethod.getInstance());
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.search_help_find_friends_dialog_title)
                        .setView(message, 100, 100, 100, 100)
                        .setCancelable(true)
                        .create()
                        .show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.primary_light));
            }
        });
        helpFindFriendsText.setMovementMethod(LinkMovementMethod.getInstance());
        helpFindFriendsText.setText(str);
    }

    /**
     * Upon launching the Search Activity, we try to enable social activity by already displaying a
     * list of random users.
     */
    private void triggerRandomQuery() {
        Random random = new Random();
        char c = (char) (random.nextInt(26) + 'a'); // 26, because there are 26 letters in the alphabet
        ArrayList<String> list = new ArrayList<>();
        list.add(Character.toString(c));
        searchForUsers(list, new FindCallback<WLUser>() {
            @Override
            public void done(List<WLUser> list, ParseException e) {
                handleSearchResults(list);
            }
        });
    }

    private ArrayList<String> parseKeywords(String sentence) {
        sentence = sentence.toLowerCase();
        return new ArrayList<>(Arrays.asList(sentence.split(" ")));
    }

    private void searchForUsers(ArrayList<String> keywordsToSearch, FindCallback<WLUser> callback) {
        ParseQuery<WLUser> query = WLUser.getQuery();
        query.whereContainsAll(WLUser.KEY_KEYWORDS, keywordsToSearch);
        query.findInBackground(callback);
    }

    /**
     * Provided a list of WLUsers, this function connects them in a many-to-many relationship by
     * adding to their following list field.
     * @param waveList The list of WLUsers to connect to each other. The list is always expected
     *                 to be small.
     */
    private void connectUsersInWave(List<WLUser> waveList) {
        for (int i = 0; i < waveList.size(); i++) {
            WLUser selectedUser = waveList.get(i);
            ParseRelation<WLUser> followingRelation = selectedUser.getRelation(WLUser.KEY_FOLLOWING);
            for (int j = 0; j < waveList.size(); j++) {
                // make sure the user does not have themself as a follower
                if (i != j) {
                    followingRelation.add(waveList.get(j));
                }
            }
            selectedUser.saveInBackground();
        }
    }

    private void addUserToWave(final WLUser selectedUser) {
        // After checking if the user hasn't already been added to the selected users, add accordingly
        boolean addUser = true;
        for (int i = 0; i < selectedUsersForWave.size(); i++) {
            if (selectedUser.getSpotifyId().equals(selectedUsersForWave.get(i).getSpotifyId())) {
                addUser = false;
                break;
            }
        }
        if (addUser) {
            selectedUsersForWave.add(selectedUser);
            waveRecyclerViewAdapter.notifyDataSetChanged();
            Log.d(TAG, selectedUser.getSpotifyId() + " added");
            // Display snackbar notifying user they have added a user to their wave
            Snackbar.make(coordinatorLayout, SNACKBAR_RESPONSE, Snackbar.LENGTH_SHORT)
                    .setAction(SNACKBAR_ACTION, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedUsersForWave.remove(selectedUser);
                            Log.d(TAG, selectedUser.getSpotifyId() + " removed");
                            waveRecyclerViewAdapter.notifyDataSetChanged();
                            if (selectedUsersForWave.isEmpty()) {
                                waveHelpText.setVisibility(View.VISIBLE);
                                waveRecyclerView.setVisibility(View.GONE);
                            }
                        }
                    })
                    .setDuration(Snackbar.LENGTH_LONG)
                    .setActionTextColor(getResources().getColor(R.color.secondary))
                    .show();

            // Remove the help text and enable my wave recyclerview
            waveHelpText.setVisibility(View.GONE);
            waveRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Helper function to display and hide views appropriately according to the search results.
     * @param results The list of WLUser search results
     */
    private void handleSearchResults(List<WLUser> results) {
        if (!results.isEmpty()) {
            helpFindFriendsText.setVisibility(View.GONE);
            peopleListTitleView.setVisibility(View.VISIBLE);
            searchResults.addAll(results);
            searchRecyclerViewAdapter.notifyDataSetChanged();
            searchBarEditText.setText(""); // set the search bar to an empty string if search was successful
        } else {
            // If there are no results from searching, display the help text option
            // and don't display the search list title
            helpFindFriendsText.setVisibility(View.VISIBLE);
            peopleListTitleView.setVisibility(View.GONE);
        }
    }

    /**
     * Closes soft keyboard. Taken from:
     * http://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
     */
    private void closeSoftKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    // endregion

    // region Interactions

    @OnClick(R.id.search_action_close)
    public void closeActionClicked() {
        finish();
    }

    @OnClick(R.id.search_action_done)
    public void doneActionClicked() {
        WLUser currentUser = WLUser.getCurrentUser(this);
        if (currentUser != null) {
            // prepend the list with the current user so he/she can be a part of the wave creation
            selectedUsersForWave.add(currentUser);
        }
        connectUsersInWave(selectedUsersForWave);

        // If the keyboard was open from searching, close it before moving to the next screen
        closeSoftKeyboard();

        // go to listening screen
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(BUNDLE_SEARCH_RESULTS_KEY, selectedUsersForWave);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.search_button)
    public void searchButtonClicked() {
        searchResults.clear();
        searchForUsers(parseKeywords(searchBarEditText.getText().toString()), new FindCallback<WLUser>() {
            @Override
            public void done(List<WLUser> list, ParseException e) {
                if (e == null) {
                    handleSearchResults(list);
                }
            }
        });
    }

    @Override
    public void horizontalBubbleRecyclerViewItemClicked(View view, int position) {
        // no op
    }

    @Override
    public void searchRecyclerViewItemClicked(View v, int position) {
        addUserToWave(searchResults.get(position));
        searchBarEditText.requestFocus();
    }

    // endregion
}
