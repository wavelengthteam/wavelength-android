package com.wavelength.start;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.wavelength.NavigationActivity;
import com.wavelength.R;
import com.wavelength.WavelengthApplication;
import com.wavelength.models.WLUser;
import com.wavelength.start.onboarding.OnboardingActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.UserPrivate;
import retrofit.client.Response;


public class LoginActivity extends AppCompatActivity {

    // region Variables

    @Bind(R.id.login_button_spotify_auth) Button loginButton;
    @Bind(R.id.login_progress_bar) ProgressBar loginProgressBar;

    public static String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static String IS_PREMIUM = "IS_PREMIUM";
    public static String AUTO_LOGIN = "LoginActivityAutoLoginBoolean";
    private static int LOGIN_MANUAL_REQUEST_CODE = 123;
    private static int LOGIN_AUTO_REQUEST_CODE = 456;
    private static int ONBOARDING_ACTIVITY_REQUEST_CODE = 789;

    AuthenticationRequest authenticationRequest;

    // endregion

    // region Fundamentals

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setUpAuthentication();

        // If it is the user's first time opening this app, open the onboarding screens and set the
        // fact that it is no longer the user's first time.
        // Otherwise, try logging the user in automatically
        if (((WavelengthApplication)getApplication()).isInitialAppLaunch()) {
            Intent newIntent = new Intent(this, OnboardingActivity.class);
            startActivityForResult(newIntent, ONBOARDING_ACTIVITY_REQUEST_CODE);
            ((WavelengthApplication)getApplication()).setInitialAppLaunch(false);
        } else {
            attemptAutomaticLogin();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        // If the user returned from the onboarding screen successfully, start a manual login.
        // Otherwise, it is likely a login response from this login activity
        if (requestCode == ONBOARDING_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            setLoginViewsState(false);
            startSpotifyLoginActivity(LOGIN_MANUAL_REQUEST_CODE);
        } else if (requestCode == LOGIN_AUTO_REQUEST_CODE
                || requestCode == LOGIN_MANUAL_REQUEST_CODE) {
            setLoginViewsState(false);
            login(AuthenticationClient.getResponse(resultCode, intent),
                    (requestCode == LOGIN_MANUAL_REQUEST_CODE));
        }
    }

    // endregion

    // region Setup

    private void setUpAuthentication() {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(getString(R.string.spotify_client_key),
                AuthenticationResponse.Type.TOKEN,
                getString(R.string.spotify_redirect_uri));
        builder.setScopes(new String[]{"user-read-private", "streaming", "playlist-read-collaborative",
                "user-read-email", "playlist-modify-public", "playlist-read-private"});
        authenticationRequest = builder.build();
    }

    // endregion

    // region Interactions

    @OnClick(R.id.login_button_spotify_auth)
    public void loginButtonClicked() {
        startSpotifyLoginActivity(LOGIN_MANUAL_REQUEST_CODE);
    }

    @OnClick(R.id.login_learn_more)
    public void learnMoreTextClicked() {
        Intent newIntent = new Intent(this, OnboardingActivity.class);
        newIntent.putExtra(OnboardingActivity.IS_EXITABLE_INTENT_KEY, true);
        startActivityForResult(newIntent, ONBOARDING_ACTIVITY_REQUEST_CODE);
    }

    // endregion

    // region Helpers

    private void setLoginViewsState(boolean isEnabled) {
        if (isEnabled) {
            loginButton.setEnabled(true);
            loginButton.setText(R.string.login_button_text_enabled);
            loginProgressBar.setVisibility(View.GONE);
        } else {
            loginButton.setEnabled(false);
            loginButton.setText(R.string.login_button_text_disabled);
            loginProgressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Creates the necessary Spotify objects from the authentication response and obtains the user.
     * Checks if the user needs to be saved or updated on the cloud, and performs any updates.
     * Eventually launches next activity.
     * @param authenticationResponse The response from Spotify's authentication activity
     * @param isManualLogin Whether the user actively is logging in, or logging in automatically.
     *                      This helps find whether users are logging in for the first time and must
     *                      go through our automatic sign up.
     */
    private void login(final AuthenticationResponse authenticationResponse, final boolean isManualLogin) {
        final Context context = this;
        SpotifyApi api = new SpotifyApi();
        api.setAccessToken(authenticationResponse.getAccessToken());
        SpotifyService spotify = api.getService();
        spotify.getMe(new SpotifyCallback<UserPrivate>() {
            @Override
            public void success(final UserPrivate me, Response response) {
                if (me != null) {
                    ParseQuery<WLUser> query = WLUser.getQuery();
                    query.whereContains(WLUser.KEY_SPOTIFY_ID, me.id);
                    query.findInBackground(new FindCallback<WLUser>() {
                        @Override
                        public void done(List<WLUser> list, ParseException e) {
                            WLUser currentUser = list.isEmpty() ?
                                    createUserFromSpotifyData(me) :
                                    list.get(0);

                            // if the login happened automatically, that means the user had already been stored
                            // from before and we bypass saving to the network. Otherwise, there is this the
                            // user's first time logging in, and we save it to the cloud.
                            if (isManualLogin || list.isEmpty()) {
                                currentUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e != null) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } else if (me.images != null &&
                                    !me.images.isEmpty() &&
                                    me.images.get(0).url != currentUser.getImageUrl()) { // If the profile image changed, update it!
                                currentUser.setImageUrl(me.images.get(0).url);
                                currentUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e != null) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }

                            WLUser.setCurrentUser(context, currentUser);

                            // Check if user is premium
                            boolean isPremium = (me.product.equals("premium"));

                            launchNavigationActivity(authenticationResponse.getAccessToken(), isPremium);
                        }
                    });
                } else {
                    handleUnsuccessfulLogin();
                }
            }

            @Override
            public void failure(SpotifyError error) {
                error.printStackTrace();
                handleUnsuccessfulLogin();
            }
        });
    }

    /**
     * This checks the conditions necessary before launching an automatic login attempt.
     * If we have the current user stored in SharedPreferences, and whatever activity we are
     * coming from has not specifically instructed that we do not attempt auto-login, then that
     * means he/she logged in recently, and we should attempt to auto-login.
     * If we are entering this activity after the user has logged out, or, if the user is
     * opening our app for the first time, it should be the case that nothing happens.
     */
    private void attemptAutomaticLogin() {
        boolean shouldAutoLogin = getIntent().getBooleanExtra(AUTO_LOGIN, true);
        if (WLUser.getCurrentUser(this) != null && shouldAutoLogin) {
            startSpotifyLoginActivity(LOGIN_AUTO_REQUEST_CODE);
        }
    }

    private void startSpotifyLoginActivity(int requestCode) {
        AuthenticationClient.openLoginActivity(this, requestCode, authenticationRequest);
    }

    private WLUser createUserFromSpotifyData(@NonNull final UserPrivate me) {
        return !me.images.isEmpty() ?
                new WLUser(me.display_name, me.id, me.email, me.images.get(0).url, null) :
                new WLUser(me.display_name, me.id, me.email, null, null);
    }

    private void launchNavigationActivity(String accessToken, boolean isPremium) {
        Intent newIntent = new Intent(this, NavigationActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        newIntent.putExtra(ACCESS_TOKEN, accessToken);
        newIntent.putExtra(IS_PREMIUM, isPremium);
        startActivity(newIntent);
    }

    /**
     * Shows a simple dialog explaining there was an unsuccessful login, and re-enables the login
     * button.
     */
    private void handleUnsuccessfulLogin() {
        final Context context = this;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle("Login Unsuccessful");

        // set dialog message
        alertDialogBuilder
                .setMessage("We don't know what happened, but maybe trying to login again will help.")
                .setCancelable(true)
                .setPositiveButton("Try Again",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        startSpotifyLoginActivity(LOGIN_MANUAL_REQUEST_CODE);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        // re enable login button
        setLoginViewsState(true);
    }

    // endregion
}