package com.wavelength.start;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavelength.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * The main fragment that handles the user starting the wave process.
 * As taken from the Android documentation: http://developer.android.com/training/basics/fragments/communicating.html
 * "All Fragment-to-Fragment communication is done through the associated Activity. Two Fragments
 * should never communicate directly."
 *
 * Depending on whether the user choose to create a wave or join a wave, this information will be
 * relayed to the associated Activity to show the next fragment.
 *
 * Currently, StartWaveFragment handles Join Wave, whereas creating a wave is handled by the
 * Activity.
 */
public class StartWaveFragment extends Fragment {

    // region Variables

    public static final String TAG = "StartWaveFragment";

    OnStartWaveDecisionSelectedListener activityCallback;

    // endregion

    // region Interfaces
    /**
     * Activity associated with this fragment must implement this listener in order for this fragment
     * to properly relay information.
     */
    public interface OnStartWaveDecisionSelectedListener {
        void onCreateWaveSelected();
    }

    // endregion

    // region Fragment methods

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This forces the activity instantiating this class to implement the corresponding listener
        // needed for this fragment to properly send events
        try {
            activityCallback = (OnStartWaveDecisionSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnStartWaveDecisionSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_wave_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // endregion

    // region User Interactions

    @OnClick(R.id.start_create_wave_button)
    public void createWave() {
        activityCallback.onCreateWaveSelected();
    }

    // endregion
}
