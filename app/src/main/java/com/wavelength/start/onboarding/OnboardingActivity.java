package com.wavelength.start.onboarding;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wavelength.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OnboardingActivity extends AppCompatActivity {

    // region Variables

    public static String IS_EXITABLE_INTENT_KEY = "isExitable";
    private static int MAX_ONBOARDING_PAGES = 4;

    @Bind(R.id.onboarding_pager) ViewPager onboardingPager;
    @Bind(R.id.onboarding_indicator_0) ImageView indicatorImageView0;
    @Bind(R.id.onboarding_indicator_1) ImageView indicatorImageView1;
    @Bind(R.id.onboarding_indicator_2) ImageView indicatorImageView2;
    @Bind(R.id.onboarding_indicator_3) ImageView indicatorImageView3;
    @Bind(R.id.onboarding_button_header) Button headerButton;

    private OnboardingPagerAdapter onboardingPagerAdapter;

    private int currentPosition = 0;
    private boolean reachedEndOfTutorial = false;

    // endregion

    // region Fundamentals

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);
        setUpOnboardingPager();
        if (getIntent().getBooleanExtra(IS_EXITABLE_INTENT_KEY, false)) {
            headerButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        boolean returnToPrevActivity = getIntent().getBooleanExtra(IS_EXITABLE_INTENT_KEY, false);
        if (returnToPrevActivity) {
            super.onBackPressed();
        } else {
            // return to the android home screen as opposed to the previous activity if it was not
            // explicitly specified that we should return to the previous activity
            Intent returnToAndroidHome = new Intent(Intent.ACTION_MAIN);
            returnToAndroidHome.addCategory(Intent.CATEGORY_HOME);
            returnToAndroidHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(returnToAndroidHome);
        }
    }

    // endregion

    // region Helpers

    private void setUpOnboardingPager() {
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        onboardingPagerAdapter = new OnboardingPagerAdapter(getSupportFragmentManager());
        onboardingPager.setAdapter(onboardingPagerAdapter);

        // Set up the page listener so we can change selected/unselected indicators
        onboardingPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // no op
            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                if (!reachedEndOfTutorial) {
                    setHeadlineButtonText(position);
                }
                switch(position) {
                    case 0:
                        indicatorImageView0.setImageResource(R.drawable.onboarding_indicator_selected);
                        indicatorImageView1.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView2.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView3.setImageResource(R.drawable.onboarding_indicator_unselected);
                        break;
                    case 1:
                        indicatorImageView0.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView1.setImageResource(R.drawable.onboarding_indicator_selected);
                        indicatorImageView2.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView3.setImageResource(R.drawable.onboarding_indicator_unselected);
                        break;
                    case 2:
                        indicatorImageView0.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView1.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView2.setImageResource(R.drawable.onboarding_indicator_selected);
                        indicatorImageView3.setImageResource(R.drawable.onboarding_indicator_unselected);
                        break;
                    case 3:
                        indicatorImageView0.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView1.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView2.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView3.setImageResource(R.drawable.onboarding_indicator_selected);

                        // When we reach the last page, we check if it is the FIRST time we're reaching
                        // the last page. If so, we animate the button to let the user know to attempt logging in.
                        if (!reachedEndOfTutorial) {
                            animateHeaderButton();
                            reachedEndOfTutorial = true;
                        }
                        break;
                    default:
                        // default to case 0
                        indicatorImageView0.setImageResource(R.drawable.onboarding_indicator_selected);
                        indicatorImageView1.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView2.setImageResource(R.drawable.onboarding_indicator_unselected);
                        indicatorImageView3.setImageResource(R.drawable.onboarding_indicator_unselected);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // no op
            }
        });
    }

    private void setHeadlineButtonText(int position) {
        switch (position) {
            case 0:
                headerButton.setText(getString(R.string.onboarding_header_button_0));
                break;
            case 1:
                headerButton.setText(getString(R.string.onboarding_header_button_1));
                break;
            case 2:
                headerButton.setText(getString(R.string.onboarding_header_button_2));
                break;
            case 3:
                headerButton.setText(getString(R.string.login_button_text_enabled));
                break;
        }
    }

    private void animateHeaderButton() {
        int animationDuration = 300;
        int repeatCount = 3;
        String xScaleKey = "scaleX";
        String yScaleKey = "scaleY";
        float scaledValue = 1.05f;
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(headerButton,
                PropertyValuesHolder.ofFloat(xScaleKey, scaledValue),
                PropertyValuesHolder.ofFloat(yScaleKey, scaledValue));
        scaleDown.setDuration(animationDuration);
        scaleDown.setRepeatCount(repeatCount);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.start();
    }

    // endregion

    // region Interactions

    @OnClick(R.id.onboarding_button_header)
    public void onHeaderButtonClicked() {
        if (!reachedEndOfTutorial && currentPosition < MAX_ONBOARDING_PAGES) {
            onboardingPager.setCurrentItem(currentPosition + 1, true);
        } else {
            // return to the login activity
            // because the only global interaction a user can have at this activity is logging in, we
            // can simply return to the LoginActivity that spawned this activity letting it know we're
            // good to login
            setResult(RESULT_OK, getIntent());
            finish();
        }
    }

    // endregion
}