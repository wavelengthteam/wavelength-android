package com.wavelength.start.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wavelength.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A dynamic {@link Fragment} that, depending on its PAGE_ID_KEY, changes what content it delivers.
 * This class is used in conjunction with {@link OnboardingActivity} and
 * {@link OnboardingPagerAdapter} to support the onboarding experience.
 */
public class OnboardingPageFragment extends Fragment {

    public static String PAGE_ID_KEY = "page_id";
    public static int NUMBER_OF_PAGES = 4;

    @Bind(R.id.onboarding_page_header) TextView headerTextView;
    @Bind(R.id.onboarding_page_description) TextView descriptionTextView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_page, container, false);
        ButterKnife.bind(this, view);
        Bundle args = getArguments();
        setUpTextViews(args.getInt(PAGE_ID_KEY));
        return view;
    }

    /**
     * Depending on the id given, sets different content for the text views this fragment inflates.
     * @param id The value that indicates which set of content to deliver.
     */
    private void setUpTextViews(int id) {
        switch(id) {
            case 0:
                headerTextView.setText(R.string.onboarding_header_0);
                descriptionTextView.setText(R.string.onboarding_desc_0);
                break;
            case 1:
                headerTextView.setText(R.string.onboarding_header_1);
                descriptionTextView.setText(R.string.onboarding_desc_1);
                break;
            case 2:
                headerTextView.setText(R.string.onboarding_header_2);
                descriptionTextView.setText(R.string.onboarding_desc_2);
                break;
            case 3:
                headerTextView.setText(R.string.onboarding_header_3);
                descriptionTextView.setText(R.string.onboarding_desc_3);
                break;
            default:
                headerTextView.setText(R.string.onboarding_header_0);
                descriptionTextView.setText(R.string.onboarding_desc_0);
                break;
        }
    }
}