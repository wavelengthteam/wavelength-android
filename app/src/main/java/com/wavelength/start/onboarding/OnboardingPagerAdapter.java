package com.wavelength.start.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * A simple adapter for {@link android.support.v4.view.ViewPager} that returns the corresponding
 * page in the onboarding tutorial.
 */
public class OnboardingPagerAdapter extends FragmentStatePagerAdapter {
    public OnboardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        // create a fragment, and give it the id of our position in the pager
        Fragment fragment = new OnboardingPageFragment();
        Bundle args = new Bundle();
        args.putInt(OnboardingPageFragment.PAGE_ID_KEY, i);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return OnboardingPageFragment.NUMBER_OF_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + (position + 1);
    }
}