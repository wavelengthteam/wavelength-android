package com.wavelength.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A Utils class to help modify Strings.
 */
public class TextUtils {

    private final static String NON_THIN = "[^iIl1\\.,']";

    private static int textWidth(String str) {
        return (int) (str.length() - str.replaceAll(NON_THIN, "").length() / 2);
    }

    public static String ellipsize(String text, int max) {

        if (textWidth(text) <= max)
            return text;

        // Start by chopping off at the word before max
        // This is an over-approximation due to thin-characters...
        int end = text.lastIndexOf(' ', max - 3);

        // Just one long word. Chop it off.
        if (end == -1)
            return text.substring(0, max-3) + "...";

        // Step forward as long as textWidth allows.
        int newEnd = end;
        do {
            end = newEnd;
            newEnd = text.indexOf(' ', end + 1);

            // No more spaces.
            if (newEnd == -1)
                newEnd = text.length();

        } while (textWidth(text.substring(0, newEnd) + "...") < max);

        return text.substring(0, end) + "...";
    }

    public static String getMinutesFromMs(long ms) {
        return String.format("%d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(ms),
                TimeUnit.MILLISECONDS.toSeconds(ms) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms))
        );
    }

    public static String createCommaSeparatedStringFromList(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for(String item : strings) {
            if(sb.length() > 0) {
                sb.append(',');
            }
            sb.append(item);
        }
        return sb.toString();
    }

    /**
     * Applies a click listener to a portion of text found in a String.
     * @param stringToSpannable The full text in which we are applying span filters to
     * @param target The portion of the full text to apply spans to
     * @param clickableSpan The click listener that will respond to click events to target
     */
    public static SpannableString applyClickableSpan(String stringToSpannable,
                                                     String target,
                                                     ClickableSpan clickableSpan) {
        // base case
        if (target.length() > stringToSpannable.length()) {
            return null;
        }
        SpannableString spannable = new SpannableString(stringToSpannable);
        final int start = stringToSpannable.indexOf(target);
        final int end = start + target.length();
        spannable.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

}
